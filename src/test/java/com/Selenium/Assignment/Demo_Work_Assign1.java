package com.Selenium.Assignment;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Demo_Work_Assign1 {
public static void main(String[] args) throws Throwable {
	WebDriverManager.chromedriver().setup();
	WebDriver driver=new ChromeDriver();
	driver.get("https://demowebshop.tricentis.com/");
	driver.manage().window().maximize();
	
	driver.findElement(By.xpath("//a[contains(text(),'Log in')]")).click();
	driver.findElement(By.id("Email")).sendKeys("hariprasathjk09@gmail.com");
	driver.findElement(By.id("Password")).sendKeys("ph@14Nov2022");
	Thread.sleep(10000);
	driver.findElement(By.xpath("//input[@value='Log in']")).click();
	
	Actions ac=new Actions(driver);
	ac.moveToElement(driver.findElement(By.xpath("(//div[@class='top-menu-triangle'])[2]//preceding::a[contains(text(),'Computers')]"))).build().perform();
	
	Robot r=new Robot();
	r.keyPress(KeyEvent.VK_DOWN);
	r.keyRelease(KeyEvent.VK_DOWN);
	Thread.sleep(10000);
	
	Actions ac1=new Actions(driver);
	ac1.doubleClick(driver.findElement(By.xpath("(//div[@class='top-menu-triangle'])[3]//preceding::a[contains(text(),'Desktops')]"))).build().perform();
	
	driver.findElement(By.xpath("//a[contains(text(),'Build your own cheap computer')]")).click();
	driver.findElement(By.xpath("//div[text()= 'Free shipping']//following::input[@value='Add to cart'][1]")).click();
	Thread.sleep(10000);
	driver.findElement(By.xpath("//span[text()= 'Shopping cart']")).click();
	
	driver.findElement(By.id("termsofservice")).click();
	driver.findElement(By.id("checkout")).click();
	
	Select sc=new Select(driver.findElement(By.id("BillingNewAddress_CountryId")));
	sc.selectByVisibleText("Canada");
	
	driver.findElement(By.id("BillingNewAddress_City")).sendKeys("Chennai");
	driver.findElement(By.id("BillingNewAddress_Address1")).sendKeys("Paul wargees street");
	driver.findElement(By.id("BillingNewAddress_Address2")).sendKeys("Paul wargees street");
	driver.findElement(By.id("BillingNewAddress_ZipPostalCode")).sendKeys("600082");
	driver.findElement(By.id("BillingNewAddress_PhoneNumber")).sendKeys("8556545261");
	
	driver.findElement(By.xpath("//input[@onclick='Billing.save()']")).click();
	
	driver.findElement(By.xpath("//input[@onclick='Shipping.save()']")).click();
	
	driver.findElement(By.xpath("//input[@onclick='ShippingMethod.save()']")).click();
	
	driver.findElement(By.xpath("//input[@onclick='PaymentMethod.save()']")).click();
	driver.findElement(By.xpath("//input[@onclick='PaymentInfo.save()']")).click();
	driver.findElement(By.xpath("//input[@onclick='ConfirmOrder.save()']")).click();
	
	WebElement text=driver.findElement(By.xpath("//*[contains(text(),'Your order has been successfully processed!')]"));
	System.out.println("Expected output came: "+text.getText());
	
	driver.findElement(By.xpath("//input[@value='Continue']")).click();
	
	driver.findElement(By.xpath("//a[contains(text(),'Log out')]")).click();
	
}
}
