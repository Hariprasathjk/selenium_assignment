package com.Selenium.Assignment;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Org_HRM_Assign4 {
	public static void main(String[] args) throws Throwable {
		WebDriverManager.chromedriver().setup();
		WebDriver driver=new ChromeDriver();
		driver.get("https://opensource-demo.orangehrmlive.com");
		driver.manage().window().maximize();
		Thread.sleep(10000);
		
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("Admin");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("admin123");
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.findElement(By.xpath("/html/body/div/div[1]/div[1]/aside/nav/div[2]/ul/li[1]/a/span")).click();
		
		driver.findElement(By.xpath("//span[contains(text(),'Organization ')]")).click();
		
		driver.findElement(By.xpath("//a[contains(text(),'Locations')]")).click();
		

		driver.findElement(By.xpath("//div[@class='orangehrm-header-container']//child::button[@type='button']")).click();
		
		driver.findElement(By.xpath("//label[contains(text(),'Name')]//following::input[@placeholder='Type here ...'][1]")).sendKeys("HariPrasath");
		
		driver.findElement(By.xpath("//label[contains(text(),'City')]//following::input[@placeholder='Type here ...'][1]")).sendKeys("Chennai");
		
		driver.findElement(By.xpath("//label[contains(text(),'State/Province')]//following::input[@placeholder='Type here ...'][1]")).sendKeys("TamilNadu");
		
		driver.findElement(By.xpath("//label[contains(text(),'Zip/Postal Code')]//following::input[@placeholder='Type here ...'][1]")).sendKeys("600052");
		
		driver.findElement(By.xpath("//label[contains(text(),'Country')]//following::i[@class='oxd-icon bi-caret-down-fill oxd-select-text--arrow'][1]")).click();
		
		Robot r=new Robot();
		r.keyPress(KeyEvent.VK_DOWN);
		r.keyRelease(KeyEvent.VK_DOWN);
		r.keyPress(KeyEvent.VK_DOWN);
		r.keyRelease(KeyEvent.VK_DOWN);
		r.keyPress(KeyEvent.VK_DOWN);
		r.keyRelease(KeyEvent.VK_DOWN);
		r.keyPress(KeyEvent.VK_DOWN);
		r.keyRelease(KeyEvent.VK_DOWN);
		
		driver.findElement(By.xpath("//label[contains(text(),'Phone')]//following::input[@placeholder='Type here ...'][1]")).sendKeys("9564782365");
		
		driver.findElement(By.xpath("//label[contains(text(),'Fax')]//following::input[@placeholder='Type here ...'][1]")).sendKeys("562355");
		
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		
driver.findElement(By.xpath("//span[@class='oxd-userdropdown-tab']")).click();
		
		driver.findElement(By.xpath("//a[contains(text(),'Logout')]")).click();
		
		
		
		
	}

}
