package com.Selenium.Assignment;

import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Demo_Work_Assign2 {
	public static void main(String[] args) throws Throwable {
		WebDriverManager.chromedriver().setup();
		WebDriver driver=new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().window().maximize();
		
		driver.findElement(By.xpath("//a[contains(text(),'Log in')]")).click();
		driver.findElement(By.id("Email")).sendKeys("hariprasathjk09@gmail.com");
		driver.findElement(By.id("Password")).sendKeys("ph@14Nov2022");
		Thread.sleep(10000);
		driver.findElement(By.xpath("//input[@value='Log in']")).click();
		
		Actions ac=new Actions(driver);
		ac.moveToElement(driver.findElement(By.xpath("(//div[@class='top-menu-triangle'])[3]//preceding::a[contains(text(),'Electronics')]"))).build().perform();
	
		Robot r=new Robot();
		r.keyPress(KeyEvent.VK_DOWN);
		r.keyRelease(KeyEvent.VK_DOWN);
		r.keyPress(KeyEvent.VK_DOWN);
		r.keyRelease(KeyEvent.VK_DOWN);
		Thread.sleep(10000);
		
		Actions ac1=new Actions(driver);
		ac1.doubleClick(driver.findElement(By.xpath("(//div[@class='top-menu-triangle'])[4]//preceding::a[contains(text(),'Cell phones')]"))).build().perform();
		
		driver.findElement(By.xpath("//a[contains(text(),'Smartphone')]")).click();
		driver.findElement(By.xpath("//div[contains(text(),'Newest Tricentis smartphone')]//following::input[@value='Add to cart'][1]")).click();
		
		Thread.sleep(10000);
		driver.findElement(By.xpath("//span[text()= 'Shopping cart']")).click();
		
		driver.findElement(By.id("termsofservice")).click();
		driver.findElement(By.id("checkout")).click();
		
		driver.findElement(By.xpath("//input[@onclick='Billing.save()']")).click();
		
		driver.findElement(By.xpath("//input[@onclick='Shipping.save()']")).click();
		
		driver.findElement(By.xpath("//input[@onclick='ShippingMethod.save()']")).click();
		
		driver.findElement(By.xpath("//input[@onclick='PaymentMethod.save()']")).click();
		driver.findElement(By.xpath("//input[@onclick='PaymentInfo.save()']")).click();
		driver.findElement(By.xpath("//input[@onclick='ConfirmOrder.save()']")).click();
		
		WebElement text=driver.findElement(By.xpath("//*[contains(text(),'Your order has been successfully processed!')]"));
		System.out.println("Expected output came: "+text.getText());
		
		driver.findElement(By.xpath("//input[@value='Continue']")).click();
		
		driver.findElement(By.xpath("//a[contains(text(),'Log out')]")).click();
	
	}

}
