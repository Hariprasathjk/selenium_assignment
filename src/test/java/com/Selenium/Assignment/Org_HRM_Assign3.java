package com.Selenium.Assignment;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Org_HRM_Assign3 {
	public static void main(String[] args) throws Throwable {
		WebDriverManager.chromedriver().setup();
		WebDriver driver=new ChromeDriver();
		driver.get("https://opensource-demo.orangehrmlive.com");
		driver.manage().window().maximize();
		Thread.sleep(10000);
		
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("Harsha");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("Hp@14Nov2022");
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.findElement(By.xpath("/html/body/div/div[1]/div[1]/aside/nav/div[2]/ul/li[1]/a/span")).click();
		
		driver.findElement(By.xpath("//div[@class='orangehrm-header-container']//child::button[@type='button']")).click();
		
		driver.findElement(By.xpath("//label[contains(text(),'User Role')]//following::i[@class='oxd-icon bi-caret-down-fill oxd-select-text--arrow'][1]")).click();
		
		Robot r=new Robot();
		r.keyPress(KeyEvent.VK_DOWN);
		r.keyRelease(KeyEvent.VK_DOWN);
		r.keyPress(KeyEvent.VK_DOWN);
		r.keyRelease(KeyEvent.VK_DOWN);
		Thread.sleep(10000);
		
		driver.findElement(By.xpath("//input[@placeholder='Type for hints...']")).sendKeys("Richard");
		
		driver.findElement(By.xpath("//label[contains(text(),'Status')]//following::i[@class='oxd-icon bi-caret-down-fill oxd-select-text--arrow'][1]")).click();
		
		Robot r1=new Robot();
		r1.keyPress(KeyEvent.VK_DOWN);
		r1.keyRelease(KeyEvent.VK_DOWN);
		r1.keyPress(KeyEvent.VK_DOWN);
		r1.keyRelease(KeyEvent.VK_DOWN);
		
		driver.findElement(By.xpath("//label[contains(text(),'Username')]//following::input[@class='oxd-input oxd-input--active'][1]")).sendKeys("Harsha");
		
		driver.findElement(By.xpath("//label[contains(text(),'Password')]//following::input[@class='oxd-input oxd-input--active'][1]")).sendKeys("hp@14Nov2022");
		
		driver.findElement(By.xpath("//label[contains(text(),'Confirm Password')]//following::input[@class='oxd-input oxd-input--active'][1]")).sendKeys("hp@14Nov2022");
		
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		
		driver.findElement(By.xpath("//span[@class='oxd-userdropdown-tab']")).click();
		
		driver.findElement(By.xpath("//a[contains(text(),'Logout')]")).click();
		
		
	}

}
